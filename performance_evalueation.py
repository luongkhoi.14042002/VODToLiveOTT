import time
from pymongo import MongoClient
import numpy as np
import matplotlib.pyplot as plt

# High frequency query example 
def get_admin_users():
    return users_collection.find({'UserRole': 'admin'})

def get_total_videos():
    return video_collection.count_documents({})

# The function executes the query and measures the average time
def execute_query(query_func, num_queries):
    total_time = 0
    success_count = 0
    failure_count = 0
    memory_usages = []
    execution_times = []  # Store execution times for line chart

    for i in range(num_queries):
        start_time = time.time()
        try:
            query_result = query_func()
            end_time = time.time()
            execution_time = end_time - start_time
            total_time += execution_time
            success_count += 1
            execution_times.append(execution_time)  # Append execution time to list
        except Exception as e:
            end_time = time.time()
            total_time += end_time - start_time
            failure_count += 1

    avg_time = total_time / num_queries
    success_rate = success_count / num_queries * 100
    response_time = total_time / (success_count + failure_count)

    return avg_time, success_count, failure_count, success_rate, response_time, execution_times

# Connect to MongoDB and get collection
client = MongoClient('mongodb+srv://14042002a:luongkhoi123@cluster0.xro9zib.mongodb.net/')
db = client['livestreamdb']
users_collection = db['User']
video_collection = db['Video']

# Lists to store data for plotting charts
query_names = ['get_admin_users', 'get_total_videos']
avg_times = []
success_rates = []
response_times = []
execution_times_list = []  # Store execution times for each query

# Execute and evaluate queries
for query_func in [get_admin_users, get_total_videos]:
    avg_time, success_count, failure_count, success_rate, response_time, execution_times = execute_query(query_func, 1000)
    avg_times.append(avg_time)
    success_rates.append(success_rate)
    response_times.append(response_time)
    execution_times_list.append(execution_times)

    print(f'Query {query_func.__name__}:')
    print('Query success count:', success_count)
    print('Query failure count:', failure_count)
    print('Query success rate:', success_rate, '%')
    print('Average response time:', response_time, 's')
    print('--------------------------------')

# Plotting the charts
plt.figure(figsize=(12, 8))

# Success Rate Chart
plt.subplot(3, 1, 1)
plt.bar(query_names, success_rates)
plt.xlabel('Queries')
plt.ylabel('Success Rate (%)')
plt.title('Success Rate Chart')

# Response Time Chart
plt.subplot(3, 1, 2)
plt.bar(query_names, response_times)
plt.xlabel('Queries')
plt.ylabel('Response Time (s)')
plt.title('Response Time Chart')

# Average Query Time Chart with Regression Lines
plt.subplot(3, 1, 3)
for i, query_name in enumerate(query_names):
    x = np.arange(1, len(execution_times_list[i]) + 1)
    y = execution_times_list[i]
    plt.plot(x, y, label=query_name)

    # Calculate and plot regression line
    z = np.polyfit(x, y, 1)
    p = np.poly1d(z)
    plt.plot(x, p(x), "r--", label="Regression Line")

plt.xlabel('Query Execution')
plt.ylabel('Execution Time (s)')
plt.title('Average Query Time Chart with Regression Lines')
plt.legend()

plt.tight_layout()
plt.show()




