import feedparser
from pymongo import MongoClient

# URL to the MRSS feed
url = 'https://www.youtube.com/feeds/videos.xml?channel_id=UC2fu6CiFfNYz5UFORvFyc0w'

# Use feedparser to fetch data from the MRSS feed
feed = feedparser.parse(url)

# Connect to MongoDB
client = MongoClient('mongodb+srv://14042002a:luongkhoi123@cluster0.xro9zib.mongodb.net/')
db = client['livestreamdb']
video_collection = db['Video']
mrss_feeds_collection = db['MRSSFeed']

# Create a dictionary to store MRSS feed data
feed_data = {
    'FeedUrl': url,
    'Author': feed.get('author', ''),
    'Title': feed.get('title', ''),
    'Link': feed.get('link', ''),
    'Published': feed.get('published', ''),
    'Updated': feed.get('updated', '')
}

# Insert MRSS feed data and get the inserted ID
mrss_feed_id = mrss_feeds_collection.insert_one(feed_data).inserted_id

# Loop through each entry in the feed and extract necessary information
for entry in feed.entries:
    video_id = entry.get('yt_videoid')
    video_title = entry.get('title')
    video_url = entry.get('link')
    published_date = entry.get('published')
    video_desc = entry.get('description')
    category = ''

    # ... add data cleaning steps if needed ...

    # Create a dictionary to store video information
    video_data = {
        'VideoID': video_id,
        'VideoTitle': video_title,
        'VideoUrl': video_url,
        'PublishedDate': published_date,
        'VideoDesc': video_desc,
        'CategoryId': category,
        'MRSSFeedId': mrss_feed_id

        # ... add other information if needed ...
    }

    # Insert video data into MongoDB
    video_collection.insert_one(video_data)

# Close MongoDB connection
client.close()
