import gevent.monkey
gevent.monkey.patch_all()

from locust import HttpUser, task, between

class MyUser(HttpUser):
    # Set the wait time between requests (1 to 5 seconds)
    wait_time = between(1, 5)  

    # Task to simulate fetching admin users
    @task
    def get_admin_users(self):
        # Send a GET request to the /admin_users endpoint
        self.client.get("/admin_users")

    # Task to simulate fetching total videos
    @task
    def get_total_videos(self):
        # Send a GET request to the /total_videos endpoint
        self.client.get("/total_videos")
