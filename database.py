from pymongo import MongoClient

# Connect to MongoDB
client = MongoClient('mongodb+srv://14042002a:luongkhoi123@cluster0.xro9zib.mongodb.net/')
db = client['livestreamdb']

# Collection: User
user_collection = db["User"]
user_collection.insert_one({
    'UserFullName': '',
    'UserPhone': '',
    'UserEmail': '',
    'UserAddress': '',
    'UserRole': ''
})

# Collection: Channel
channel_collection = db["Channel"]
channel_collection.insert_one({
    'ChannelName': '',
    'ChannelDesc': '',
    'ChannelStatus': '',
    'UserID': ''
})

# Collection: MRSSFeed
mrss_feed_collection = db["MRSSFeed"]
mrss_feed_collection.insert_one({
    'FeedUrl': '',
    'Author': '',
    'Title': '',
    'Link': '',
    'Published': '',
    'Updated': ''
})

# Collection: Video
video_collection = db["Video"]
video_collection.insert_one({
    'VideoID': '',
    'VideoTitle': '',
    'VideoUrl': '',
    'PublishedDate': '',
    'VideoDesc': '',
    'CategoryId': '',
    'MRSSFeedId': ''
})

# Collection: Comment
comment_collection = db["Comment"]
comment_collection.insert_one({
    'CmtCreated': '',
    'CmtText': '',
    'UserID': '',
    'VideoID': ''
})

# Collection: Like
like_collection = db["Like"]
like_collection.insert_one({
    'UserID': '',
    'VideoID': ''
})

# Collection: Dislike
dislike_collection = db["Dislike"]
dislike_collection.insert_one({
    'UserID': '',
    'VideoID': ''
})

# Collection: Schedule
schedule_collection = db["Schedule"]
schedule_collection.insert_one({
    'ScheduleStartTime': '',
    'ScheduleEndTime': '',
    'VideoID': '',
    'ChannelID': ''
})

# Collection: Category
category_collection = db["Category"]
category_collection.insert_one({
    'CategoryName': ''
})