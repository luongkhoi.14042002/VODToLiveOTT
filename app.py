from flask import Flask
from routes import user_routes, video_routes, category_routes, comment_routes, like_routes, dislike_routes, schedule_routes, mrss_feed_routes, channel_routes

app = Flask(__name__)

app.register_blueprint(user_routes.user_bp)
app.register_blueprint(video_routes.video_bp)
app.register_blueprint(category_routes.category_bp)
app.register_blueprint(comment_routes.comment_bp)
app.register_blueprint(like_routes.like_bp)
app.register_blueprint(dislike_routes.dislike_bp)
app.register_blueprint(schedule_routes.schedule_bp)
app.register_blueprint(mrss_feed_routes.mrss_feed_bp)
app.register_blueprint(channel_routes.channel_bp)

if __name__ == '__main__':
    app.run()
