# VOD to Live OTT Content Management System

Welcome to the VOD to Live OTT Content Management System! This project aims to provide a platform for managing video-on-demand content and live streaming for an Over-The-Top (OTT) application.

## Technologies Used

- Python and Flask: Backend framework for building APIs and handling requests.
- MongoDB Atlas: NoSQL database for storing user data, videos, comments, and more.
- Locust: Load testing framework for evaluating system performance.

## Installation

1. Clone the repository: `git clone https://gitlab.com/luongkhoi.14042002/VODToLiveOTT.git`
2. Set up MongoDB: Update the connection string and database name in `db_connector.py`.
3. Run the application: `python app.py`

## API Routes

The following API routes are available:

- `/users`: Get a list of all users.
- `/videos`: Get a list of all videos.
- `/categories`: Get a list of all categories.
- `/comments`: Get a list of all comments.
- `/add_comment`: Add a new comment.
- `/delete_comment/<comment_id>`: Delete a comment.
- `/likes`: Get a list of all likes.
- `/add_like`: Add a new like.
- `/delete_like/<like_id>`: Delete a like.
- `/dislikes`: Get a list of all dislikes.
- `/add_dislike`: Add a new dislike.
- `/delete_dislike/<dislike_id>`: Delete a dislike.
- `/schedules`: Get a list of all schedules.
- `/mrss_feeds`: Get a list of all MRSS feeds.
- `/channels`: Get a list of all channels.

## Usage

- Access the API routes using your browser or tools like Postman.

## Contributing

Contributions are welcome! If you have any suggestions or find a bug, please open an issue or submit a pull request.
