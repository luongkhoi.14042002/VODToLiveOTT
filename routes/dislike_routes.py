from flask import Blueprint, jsonify, request
from pymongo import MongoClient
from bson.objectid import ObjectId
from config import CONNECTION_STR, DATABASE_NAME

dislike_bp = Blueprint('dislike_bp', __name__)

# Connect to MongoDB
client = MongoClient(CONNECTION_STR)
db = client[DATABASE_NAME]
dislike_collection = db['Dislike']

# Define API routes for Dislike collection
@dislike_bp.route('/dislikes', methods=['GET'])
def get_dislikes():
    data = list(dislike_collection.find({}, {'_id': 1, 'VideoID': 1, 'UserID': 1}))
    for item in data:
        if '_id' in item:
            item['_id'] = str(item['_id'])
        if 'UserID' in item:
            item['UserID'] = str(item['UserID'])
        if 'VideoID' in item:
            item['VideoID'] = str(item['VideoID'])
    return jsonify(data)

@dislike_bp.route('/add_dislike', methods=['POST'])
def add_dislike():
    data = request.get_json()
    user_id = data.get('UserID')
    video_id = data.get('VideoID')
    
    dislike_data = {
        'UserID': user_id,
        'VideoID': video_id
    }
    inserted_id = dislike_collection.insert_one(dislike_data).inserted_id
    return jsonify({'message': 'Dislike added successfully', 'inserted_id': str(inserted_id)})

@dislike_bp.route('/delete_dislike/<dislike_id>', methods=['DELETE'])
def delete_dislike(dislike_id):
    result = dislike_collection.delete_one({'_id': ObjectId(dislike_id)})
    if result.deleted_count > 0:
        return jsonify({'message': 'Dislike deleted successfully'})
    else:
        return jsonify({'message': 'Dislike not found'})
