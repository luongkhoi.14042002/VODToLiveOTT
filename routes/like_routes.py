from flask import Blueprint, jsonify, request
from pymongo import MongoClient
from bson.objectid import ObjectId
from config import CONNECTION_STR, DATABASE_NAME

like_bp = Blueprint('like_bp', __name__)

# Connect to MongoDB
client = MongoClient(CONNECTION_STR)
db = client[DATABASE_NAME]
like_collection = db['Like']

# Define API routes for Like collection
@like_bp.route('/likes', methods=['GET'])
def get_likes():
    data = list(like_collection.find({}, {'_id': 1, 'VideoID': 1, 'UserID': 1}))
    for item in data:
        if '_id' in item:
            item['_id'] = str(item['_id'])
        if 'UserID' in item:
            item['UserID'] = str(item['UserID'])
        if 'VideoID' in item:
            item['VideoID'] = str(item['VideoID'])
    return jsonify(data)

@like_bp.route('/add_like', methods=['POST'])
def add_like():
    data = request.get_json()
    user_id = data.get('UserID')
    video_id = data.get('VideoID')
    
    like_data = {
        'UserID': user_id,
        'VideoID': video_id
    }
    inserted_id = like_collection.insert_one(like_data).inserted_id
    return jsonify({'message': 'Like added successfully', 'inserted_id': str(inserted_id)})

@like_bp.route('/delete_like/<like_id>', methods=['DELETE'])
def delete_like(like_id):
    result = like_collection.delete_one({'_id': ObjectId(like_id)})
    if result.deleted_count > 0:
        return jsonify({'message': 'Like deleted successfully'})
    else:
        return jsonify({'message': 'Like not found'})
