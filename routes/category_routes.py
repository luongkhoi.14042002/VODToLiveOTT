from flask import Blueprint, jsonify
from pymongo import MongoClient
from config import CONNECTION_STR, DATABASE_NAME

category_bp = Blueprint('category_bp', __name__)

# Connect to MongoDB
client = MongoClient(CONNECTION_STR)
db = client[DATABASE_NAME]
category_collection = db['Category']

# Define API routes for Category collection
@category_bp.route('/categories', methods=['GET'])
def get_categories():
    data = list(category_collection.find({}, {'_id': 1, 'CategoryName': 1}))
    for item in data:
        if '_id' in item:
            item['_id'] = str(item['_id'])
    return jsonify(data)
