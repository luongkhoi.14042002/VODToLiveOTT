from flask import Blueprint, jsonify
from pymongo import MongoClient
from config import CONNECTION_STR, DATABASE_NAME

video_bp = Blueprint('video_bp', __name__)

# Connect to MongoDB
client = MongoClient(CONNECTION_STR)
db = client[DATABASE_NAME]
video_collection = db['Video']

# Define API routes for Video collection
@video_bp.route('/videos', methods=['GET'])
def get_videos():
    data = list(video_collection.find({}, {'_id': 1, 'VideoID': 1, 'VideoTitle': 1, 'VideoUrl': 1, 'PublishedDate': 1, 'VideoDesc': 1, 'CategoryId': 1, 'MRSSFeedId': 1}))
    for item in data:
        if '_id' in item:
            item['_id'] = str(item['_id'])
        if 'CategoryId' in item:
            item['CategoryId'] = str(item['CategoryId'])
        if 'MRSSFeedId' in item:
            item['MRSSFeedId'] = str(item['MRSSFeedId'])
    return jsonify(data)
