from flask import Blueprint, jsonify
from pymongo import MongoClient
from config import CONNECTION_STR, DATABASE_NAME

mrss_feed_bp = Blueprint('mrss_feed_bp', __name__)

# Connect to MongoDB
client = MongoClient(CONNECTION_STR)
db = client[DATABASE_NAME]
mrss_feed_collection = db['MRSSFeed']

# Define API routes for MRSSFeed collection
@mrss_feed_bp.route('/mrss_feeds', methods=['GET'])
def get_mrss_feeds():
    data = list(mrss_feed_collection.find({}, {'_id': 1, 'FeedUrl': 1, 'Author': 1, 'Title': 1, 'Link': 1, 'Published': 1, 'Updated': 1}))
    for item in data:
        if '_id' in item:
            item['_id'] = str(item['_id'])
    return jsonify(data)
