from flask import Blueprint, jsonify
from pymongo import MongoClient
from config import CONNECTION_STR, DATABASE_NAME

user_bp = Blueprint('user_bp', __name__)

# Connect to MongoDB
client = MongoClient(CONNECTION_STR)
db = client[DATABASE_NAME]
users_collection = db['User']

# Define API routes for User collection
@user_bp.route('/users', methods=['GET'])
def get_users():
    data = list(users_collection.find({}, {'_id': 1, 'UserFullName': 1, 'UserPhone': 1, 'UserEmail': 1, 'UserAddress': 1, 'UserRole': 1}))
    for item in data:
        if '_id' in item:
            item['_id'] = str(item['_id'])
    return jsonify(data)
