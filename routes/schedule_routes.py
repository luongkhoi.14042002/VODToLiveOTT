from flask import Blueprint, jsonify
from pymongo import MongoClient
from config import CONNECTION_STR, DATABASE_NAME

schedule_bp = Blueprint('schedule_bp', __name__)

# Connect to MongoDB
client = MongoClient(CONNECTION_STR)
db = client[DATABASE_NAME]
schedule_collection = db['Schedule']

# Define API routes for Schedule collection
@schedule_bp.route('/schedules', methods=['GET'])
def get_schedules():
    data = list(schedule_collection.find({}, {'_id': 1, 'ScheduleStartTime': 1, 'ScheduleEndTime': 1, 'VideoID': 1, 'ChannelID': 1}))
    for item in data:
        if '_id' in item:
            item['_id'] = str(item['_id'])
        if 'ChannelID' in item:
            item['ChannelID'] = str(item['ChannelID'])
        if 'VideoID' in item:
            item['VideoID'] = str(item['VideoID'])
    return jsonify(data)
