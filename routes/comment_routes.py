from flask import Blueprint, jsonify, request
from pymongo import MongoClient
from bson.objectid import ObjectId
from config import CONNECTION_STR, DATABASE_NAME

comment_bp = Blueprint('comment_bp', __name__)

# Connect to MongoDB
client = MongoClient(CONNECTION_STR)
db = client[DATABASE_NAME]
comment_collection = db['Comment']

# Define API routes for Comment collection
@comment_bp.route('/comments', methods=['GET'])
def get_comments():
    data = list(comment_collection.find({}, {'_id': 1, 'CmtCreated': 1, 'CmtText': 1, 'VideoID': 1, 'UserID': 1}))
    for item in data:
        if '_id' in item:
            item['_id'] = str(item['_id'])
        if 'UserID' in item:
            item['UserID'] = str(item['UserID'])
        if 'VideoID' in item:
            item['VideoID'] = str(item['VideoID'])
    return jsonify(data)

@comment_bp.route('/add_comment', methods=['POST'])
def add_comment():
    data = request.get_json()
    cmt_created = data.get('CmtCreated')
    cmt_text = data.get('CmtText')
    
    comment_data = {
        'CmtCreated': cmt_created,
        'CmtText': cmt_text
    }
    inserted_id = comment_collection.insert_one(comment_data).inserted_id
    return jsonify({'message': 'Comment added successfully', 'inserted_id': str(inserted_id)})

@comment_bp.route('/delete_comment/<comment_id>', methods=['DELETE'])
def delete_comment(comment_id):
    result = comment_collection.delete_one({'_id': ObjectId(comment_id)})
    if result.deleted_count > 0:
        return jsonify({'message': 'Comment deleted successfully'})
    else:
        return jsonify({'message': 'Comment not found'})
