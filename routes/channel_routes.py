from flask import Blueprint, jsonify
from pymongo import MongoClient
from config import CONNECTION_STR, DATABASE_NAME

channel_bp = Blueprint('channel_bp', __name__)

# Connect to MongoDB
client = MongoClient(CONNECTION_STR)
db = client[DATABASE_NAME]
channel_collection = db['Channel']

# Define API routes for Channel collection
@channel_bp.route('/channels', methods=['GET'])
def get_channels():
    data = list(channel_collection.find({}, {'_id': 1, 'ChannelName': 1, 'ChannelDesc': 1, 'ChannelStatus': 1, 'UserID': 1}))
    for item in data:
        if '_id' in item:
            item['_id'] = str(item['_id'])
        if 'UserID' in item:
            item['UserID'] = str(item['UserID'])
    return jsonify(data)
